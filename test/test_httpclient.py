import pytest
from cocoricofm import httpclient
import http.server
import socketserver
import asyncio
import json
from gi.events import GLibEventLoopPolicy

PORT = 8002
URL = f'http://localhost:{PORT}'

socketserver.TCPServer.allow_reuse_address = True
asyncio.set_event_loop_policy(GLibEventLoopPolicy())

class Handler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        data = b'Hello friends'
        if self.path == '/invalid.json':
            data = rb'{"composer": "Phil Collen, Robert John \"Mutt\" Lange, Joe Elliott, Rick Savage, Steve Clark"}'
        self.send_response(200)
        self.end_headers()
        self.wfile.write(data)
        self.wfile.write(b'')

class TestHTTPClient:

    def setup_method(self, method):
        self.httpd = socketserver.TCPServer(("localhost", PORT), Handler)
        self.f = asyncio.ensure_future(asyncio.to_thread(self.httpd.serve_forever))

    def teardown_method(self, method):
        self.httpd.shutdown()
        self.f.cancel()

    @pytest.mark.asyncio
    async def test_invalid_json(self):
        client = httpclient.HttpClient()
        response = await client.get(f'{URL}/invalid.json')
        assert response == '{"composer": "Phil Collen, Robert John \\"Mutt\\" Lange, Joe Elliott, Rick Savage, Steve Clark"}'
        payload = json.loads(response)
        assert payload == {'composer': 'Phil Collen, Robert John "Mutt" Lange, Joe Elliott, Rick Savage, Steve Clark'}
