
from asyncio import start_server, get_event_loop, open_connection

class TCPSocket:

    def __init__(self, port=None):
        self.port = port or 8888

    async def client_request(self, message, host):
        try:
            reader_sock, writer_sock = await open_connection(host=host, port=self.port)
            writer_sock.write(message)

            reply = await reader_sock.read()
            if reply:
                print(reply.decode('utf8'))
            writer_sock.close()

        except ConnectionRefusedError:
            print("No socket opened by server")
        except FileNotFoundError:
            print("Socket is not exists")

    def start_server_task(self, callback):
        self.client_handler = callback
        loop = get_event_loop()
        return loop.create_task(start_server(self._handle_request, port=self.port))

    async def _handle_request(self, reader, writer):
        data = await reader.read(512)
        payload = self.client_handler(data)
        if payload:
            writer.write(payload)
        writer.write_eof()

def client():
    import sys

    args = sys.argv[1:]
    loop = get_event_loop()
    c = TCPSocket()
    task = c.client_request(bytes(" ".join(args[1:]), encoding='ascii'), args[0])
    loop.run_until_complete(task)

if __name__ == '__main__':
    client()
