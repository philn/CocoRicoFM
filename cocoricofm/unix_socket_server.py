
from asyncio import start_unix_server, get_event_loop, open_unix_connection

class UnixSocket:

    def __init__(self, socket_path=None):
        self.socket_path = socket_path or '/tmp/cocoricofm.socket'
        self.loop = get_event_loop()

    async def client_request(self, message):
        try:
            reader_sock, writer_sock = await open_unix_connection(path=self.socket_path, loop=self.loop)
            writer_sock.write(message)

            reply = await reader_sock.read()
            if reply:
                print(reply)
            writer_sock.close()

        except ConnectionRefusedError:
            print("No socket opened by server")
        except FileNotFoundError:
            print("Socket is not exists")

    def start_server_task(self, callback):
        self.client_handler = callback
        return self.loop.create_task(start_unix_server(self._handle_request, path=self.socket_path))

    async def _handle_request(self, reader, writer):
        data = await reader.read(512)
        payload = self.client_handler(data)
        if payload:
            writer.write(payload)
        writer.write_eof()

if __name__ == '__main__':
    import sys

    loop = get_event_loop()
    c = UnixSocket()
    if len(sys.argv[1:]) >= 1:
        task = c.client_request(bytes("tune %s" % sys.argv[1], encoding='ascii'))
    else:
        task = c.client_request(bytes("stations", encoding='ascii'))
    loop.run_until_complete(task)
