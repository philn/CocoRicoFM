# -*- coding: utf-8 -*-
# This file is part of CocoRicoFM.
#
# CocoRicoFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CocoRicoFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CocoRicoFM.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gio, GLib

Variant = GLib.Variant

# MPRIS 2.2 partial introspection data.
MPRIS_INTROSPECTION = '''\
<node>
  <interface name='org.mpris.MediaPlayer2'>
    <method name='Raise' />
    <method name='Quit'>
      <annotation name='org.freedesktop.DBus.Method.NoReply' value='true' />
    </method>
    <property name='CanQuit' type='b' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
    <property name='FullScreen' type='b' access='readwrite' />
    <property name='CanSetFullScreen' type='b' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
    <property name='CanRaise' type='b' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
    <property name='HasTrackList' type='b' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
    <property name='Identity' type='s' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
    <property name='DesktopEntry' type='s' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
    <property name='SupportedUriSchemes' type='as' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
    <property name='SupportedMimeTypes' type='as' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
  </interface>
  <interface name='org.mpris.MediaPlayer2.Player'>
    <method name='Next' />
    <method name='Previous' />
    <method name='Pause' />
    <method name='PlayPause' />
    <method name='Stop' />
    <method name='Play' />
    <method name='Seek'>
      <arg name='Offset' type='x' direction='in' />
    </method>
    <method name='SetPosition'>
      <arg name='TrackId' type='o' direction='in' />
      <arg name='Position' type='x' direction='in' />
    </method>
    <method name='OpenUri'>
      <arg name='Uri' type='s' direction='in' />
    </method>
    <signal name='Seeked'>
      <arg name='Position' type='x' />
    </signal>
    <property name='PlaybackStatus' type='s' access='read' />
    <property name='LoopStatus' type='s' access='readwrite' />
    <property name='Rate' type='d' access='readwrite' />
    <property name='Shuffle' type='b' access='readwrite' />
    <property name='Metadata' type='a{sv}' access='read' />
    <property name='Volume' type='d' access='readwrite' />
    <property name='Position' type='x' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='false' />
    </property>
    <property name='MinimumRate' type='d' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
    <property name='MaximumRate' type='d' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
    <property name='CanGoNext' type='b' access='read' />
    <property name='CanGoPrevious' type='b' access='read' />
    <property name='CanPlay' type='b' access='read' />
    <property name='CanPause' type='b' access='read' />
    <property name='CanSeek' type='b' access='read' />
    <property name='CanControl' type='b' access='read'>
      <annotation name='org.freedesktop.DBus.Property.EmitsChangedSignal' value='const' />
    </property>
  </interface>
</node>
'''

class MprisService:
    def __init__(self, controller):
        nodeinfo = Gio.DBusNodeInfo.new_for_xml(MPRIS_INTROSPECTION)
        self.root_interface = nodeinfo.interfaces[0]
        self.player_interface = nodeinfo.interfaces[1]
        self.controller = controller
        self.connection = None
        self.object = None
        self.registrations = []
        Gio.bus_own_name(Gio.BusType.SESSION, 'org.mpris.MediaPlayer2.cocoricofm', Gio.BusNameOwnerFlags.NONE,
                         self._on_bus_acquired, None, None)

    def _on_bus_acquired(self, connection, name):
        self.connection = connection
        self.object = MprisObject(self.controller, connection)
        self.registrations.append(connection.register_object('/org/mpris/MediaPlayer2', self.root_interface,
                                                             self.object.method_call,
                                                             self.object.get_property,
                                                             self.object.set_property))
        self.registrations.append(connection.register_object('/org/mpris/MediaPlayer2', self.player_interface,
                                                             self.object.method_call,
                                                             self.object.get_property,
                                                             self.object.set_property))

class MprisObject:
    def __init__(self, controller, connection):
        self.controller = controller
        self.connection = connection
        self.controller.connect('current-song-updated', self._on_notify_current_song)
        self.controller.player.connect('suspended', self._on_player_status_changed)
        self.controller.player.connect('resumed', self._on_player_status_changed)
        self.controller.player.connect('volume-changed', self._on_player_volume_changed)

    def method_call(self, connection, sender, path, interface, method, args, invocation):
        try:
            result = getattr(self, method)(*args)
        except AttributeError:
            result = None
        if result is None:
            result = Variant.new_tuple()
        elif isinstance(result, tuple):
            result = Variant.new_tuple(*result)
        else:
            result = Variant.new_tuple(result)
        invocation.return_value(result)

    def get_property(self, connection, sender, path, interface, prop):
        return getattr(self, prop)

    def set_property(self, connection, sender, path, interface, prop, value):
        setattr(self, prop, value)
        return True

    def _emit_property_change(self, interface, changed_props={}, invalidated_props=[]):
        self.connection.emit_signal(None, '/org/mpris/MediaPlayer2',
                                    'org.freedesktop.DBus.Properties', 'PropertiesChanged',
                                    Variant.new_tuple(Variant('s', interface),
                                                      Variant('a{sv}', changed_props),
                                                      Variant('as', invalidated_props)))

    def _get_metadata(self):
        infos = self.controller.current_song_infos
        if infos.is_empty():
            return {}

        meta = {'mpris:trackid': Variant('o', '/org/cocoricofm/track/%d' % id(infos))}
        if infos.duration:
            meta['mpris:length'] = Variant('x', infos.duration * 1e6)
        if infos.cover_url:
            meta['mpris:artUrl'] = Variant('s', infos.cover_url)
        if infos.album:
            meta['xesam:album'] = Variant('s', infos.album)
        if infos.artist:
            meta['xesam:artist'] = Variant('as', [infos.artist,])
        if infos.title:
            meta['xesam:title'] = Variant('s', infos.title)

        return meta

    def _on_notify_current_song(self, *args):
        self._emit_property_change('org.mpris.MediaPlayer2.Player', {'Metadata': self.Metadata})

    def _on_player_status_changed(self, *args):
        self._emit_property_change('org.mpris.MediaPlayer2.Player', {'PlaybackStatus': self.PlaybackStatus})

    def _on_player_volume_changed(self, *args):
        self._emit_property_change('org.mpris.MediaPlayer2.Player', {'Volume': self.Volume})

    def _return_true(self):
        return Variant('b', True)

    def _return_false(self):
        return Variant('b', False)

    CanRaise = CanQuit = CanControl = CanPause = CanPlay = property(_return_true)
    CanSetFullScreen = HasTrackList = FullScreen = CanGoNext = CanGoPrevious = CanSeek = property(_return_false)

    @property
    def DesktopEntry(self):
        return Variant('s', 'cocoricofm')

    @property
    def Identity(self):
        return Variant('s', 'CocoRicoFM')

    @property
    def SupportedMimeTypes(self):
        return Variant('as', [])

    @property
    def SupportedUriSchemes(self):
        return Variant('as', ['http', 'https'])

    def Raise(self):
        ui = self.controller.ui
        if ui:
            ui.present()

    def Quit(self):
        self.controller.main_quit()

    @property
    def LoopStatus(self):
        return Variant('s', 'None')

    @LoopStatus.setter
    def LoopStatus(self, value):
        pass

    @property
    def MaximumRate(self):
        return Variant('d', 1)

    @property
    def Metadata(self):
        return Variant('a{sv}', self._get_metadata())

    @property
    def MinimumRate(self):
        return Variant('d', 1)

    @property
    def PlaybackStatus(self):
        if self.controller.player.playing:
            return Variant('s', 'Playing')
        else:
            return Variant('s', 'Stopped')

    @property
    def Position(self):
        return Variant('x', 1 * 1e6)

    @property
    def Rate(self):
        return Variant('d', 1)

    @Rate.setter
    def Rate(self, value):
        pass

    @property
    def Shuffle(self):
        return Variant('b', False)

    @Shuffle.setter
    def Shuffle(self, value):
        pass

    @property
    def Volume(self):
        return Variant('d', self.controller.player.volume)

    @Volume.setter
    def Volume(self, value):
        self.controller.player.set_volume(value.get_double())

    def OpenUri(self, uri):
        pass

    def Pause(self):
        self.controller.player.pause()

    def Play(self):
        self.controller.player.start()

    def PlayPause(self):
        self.controller.player.toggle_play()

    def Seek(self, offset):
        pass

    def Stop(self):
        self.controller.player.stop()
