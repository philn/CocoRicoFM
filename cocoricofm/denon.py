# This file is part of CocoRicoFM.
#
# CocoRicoFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CocoRicoFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CocoRicoFM.  If not, see <http://www.gnu.org/licenses/>.

import asyncio

try:
    import soco
except ImportError:
    soco = None

class DenonRemote:

    def __init__(self, address, enable_sonos):
        self.address = address
        if enable_sonos and not soco:
            print("soco not found, install it with pip. Now disabling sonos support")
            enable_sonos = False
        self.enable_sonos = enable_sonos

    @property
    def enabled(self):
        return self.address != None

    async def run(self, argv):
        if len(argv) > 1:
            func = getattr(self, argv[1])
            args = argv[2:]
        else:
            cmd = argv[0].split('-')[1]
            func = getattr(self, f"toggle_{cmd}")
            args = ()
        result = await func(*args)
        if result is not None:
            print(result)

    async def _connect(self):
        if not self.enabled:
            fut = asyncio.Future()
            fut.set_result((None, None))
            return await fut

        return await asyncio.open_connection(self.address, 23)

    async def send_command(self, cmd, reader, writer):
        payload = "%s\r" % cmd
        writer.write(payload.encode('ascii'))
        await writer.drain()
        return await reader.readuntil(b'\r')

    async def power_on(self, reader, writer):
        return await self.send_command("PWON", reader, writer)

    async def power_off(self, reader, writer):
        return await self.send_command("PWSTANDBY", reader, writer)

    async def toggle_power(self):
        fut = asyncio.Future()
        reader, writer = await self._connect()
        status = await self.send_command("PW?", reader, writer)
        if status.startswith(b"PWON"):
            await self.power_off(reader, writer)
            powered = False
        else:
            await self.power_on(reader, writer)
            powered = True

        fut.set_result(powered)
        writer.close()
        await writer.wait_closed()
        return await fut

    async def send_single_command(self, name):
        reader, writer = await self._connect()
        result = await self.send_command(name, reader, writer)
        writer.close()
        await writer.wait_closed()
        return result

    async def increment_volume(self):
        return await self.send_single_command("MVUP")

    async def decrement_volume(self):
        return await self.send_single_command("MVDOWN")

    async def toggle_sequence(self, cmds):
        fut = asyncio.Future()
        reader, writer = await self._connect()
        results = []
        for (cmd, space) in cmds:
            # Skip command if previous one lead to something being turned off.
            if results and not results[-1]:
                results.append(True)
                continue
            status = await self.send_command(f"{cmd}?", reader, writer)
            result = status.strip().decode('ascii')
            cmd_str = f"{cmd} " if space else cmd
            on_str = f"{cmd_str}ON"
            if result.startswith(on_str):
                await self.send_command(f"{cmd_str}OFF", reader, writer)
                cmd_result = False
            else:
                await self.send_command(f"{cmd_str}ON", reader, writer)
                cmd_result = True
            results.append(cmd_result)

        fut.set_result(results)
        writer.close()
        await writer.wait_closed()
        return await fut

    async def toggle_mute(self):
        return await self.toggle_sequence([("MU", False)])

    async def volume(self):
        payload = await self.send_single_command("MV?")
        str_value = payload.strip()[2:]
        if len(str_value) == 3:
            fp_value = str_value[:2] + b'.' + str_value[2:]
            str_value = fp_value

        return float(str_value)

    async def set_volume(self, volume):
        # Seems like protocol doesn't allow floating point values.
        volume = int(float(volume))
        await self.send_single_command(f"MV{volume}")

    async def toggle_zone2_multi(self):
        """
        Enable Zone2 simultaneous playback with Zone1. Or not.
        """
        results = await self.toggle_sequence([("Z2", False), ("MNZST", True)])
        zone2_on = results[0]
        self.sonos_switch(zone2_on)

    def sonos_switch(self, enable):
        if not self.enable_sonos:
            return

        # This is specific to my setup. Should be moved to a user script later on.
        port = soco.discovery.by_name('Living Room')
        move = soco.discovery.by_name('Move 2')
        port.switch_to_line_in()
        move.join(port)
        if enable:
            port.play()
        else:
            port.stop()

    async def ensure(self, reader, writer, cmd, value, space_between_cmd_and_value=False):
        status = await self.send_command(f"{cmd}?", reader, writer)
        if not status.strip().endswith(value):
            value_str = value.decode('ascii')
            sep = " " if space_between_cmd_and_value else ""
            await self.send_command(f"{cmd}{sep}{value_str}", reader, writer)

    async def toggle_active_zone(self):
        """
        Either:
           - zone2 off
           - zone1 un-muted
        Or:
           - zone2 on
           - zone1 muted

        Also, we assume Zone 2 is for Sonos stuff. Again, could be tweaked later on if needed.
        """
        reader, writer = await self._connect()
        status = await self.send_command("MU?", reader, writer)
        if status.startswith(b"MUON"):
            await self.send_command("MUOFF", reader, writer)
            await self.ensure(reader, writer, "Z2", b"OFF")
            enable_sonos = False
        else:
            await self.send_command("MUON", reader, writer)
            await self.ensure(reader, writer, "Z2", b"ON")
            await self.ensure(reader, writer, "MNZST", b"ON", True)
            enable_sonos = True

        try:
            self.sonos_switch(enable_sonos)
        except Exception as exc:
            print(exc)
        writer.close()
        await writer.wait_closed()

    async def restore_zone2(self):
        """
        When zone2 is enabled and the player switches to a different station
        the multi-zone mode gets disabled (sometimes). Some bug on this denon device,
        probably... So workaround, turn it off and on again...
        """
        reader, writer = await self._connect()
        status = await self.send_command("Z2?", reader, writer)
        if status.startswith(b"Z2ON"):
            status = await self.send_command("MNZST?", reader, writer)
            if status.strip().endswith(b"ON"):
                await self.send_command("MNZST OFF", reader, writer)
                await self.send_command("MNZST ON", reader, writer)
        writer.close()
        await writer.wait_closed()

if __name__ == '__main__':
    import sys
    from gi.events import GLibEventLoopPolicy
    asyncio.set_event_loop_policy(GLibEventLoopPolicy())

    d = DenonRemote(sys.argv[1], False)

    async def main():
        status = await d.toggle_power()
        print(status)
        await asyncio.sleep(1)
        status = await d.toggle_power()
        print(status)

    f = asyncio.ensure_future(main())
    loop = asyncio.get_event_loop()
    loop.run_until_complete(f)
