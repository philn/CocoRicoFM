# -*- coding: utf-8 -*-
# This file is part of CocoRicoFM.
#
# CocoRicoFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CocoRicoFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CocoRicoFM.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import GObject, Gio

HAS_NO_CONNECTIVITY = 1
HAS_CONNECTIVITY = 4

class LinuxNetworkMonitor(GObject.GObject):
    __gsignals__ = { 'connectivity-lost': (GObject.SignalFlags.RUN_FIRST, GObject.TYPE_NONE, ()),
                     'connectivity-restored': (GObject.SignalFlags.RUN_FIRST, GObject.TYPE_NONE, ()),
    }

    def __init__(self):
        super(LinuxNetworkMonitor, self).__init__()
        self.bus = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)
        self.dbus_proxy = Gio.DBusProxy.new_sync(self.bus, Gio.DBusProxyFlags.NONE, None,
                                                 'org.freedesktop.NetworkManager',
                                                 '/org/freedesktop/NetworkManager',
                                                 'org.freedesktop.NetworkManager', None)
        connectivity = self.dbus_proxy.get_cached_property('Connectivity')
        if not connectivity:
            raise Exception("NetworkManager not running")
        self._has_connectivity = connectivity.get_uint32() == HAS_CONNECTIVITY
        self.dbus_proxy.connect('g-properties-changed', self._on_properties_changed)

    def _on_properties_changed(self, proxy, changed_properties, invalidated_properties):
        if 'Connectivity' not in changed_properties.keys():
            return

        connectivity = changed_properties['Connectivity']
        if connectivity == HAS_NO_CONNECTIVITY:
            self._has_connectivity = False
            self.emit('connectivity-lost')
        elif connectivity == HAS_CONNECTIVITY:
            self._has_connectivity = True
            self.emit('connectivity-restored')

    @property
    def has_connectivity(self):
        return self._has_connectivity
