# This file is part of CocoRicoFM.
#
# CocoRicoFM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CocoRicoFM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CocoRicoFM.  If not, see <http://www.gnu.org/licenses/>.

import asyncio

try:
    import evdev
except ImportError:
    evdev = None

class EvDevInput:
    task = None

    def __init__(self, config, controller):
        if not evdev:
            return

        self._remote_enabled = True
        self._controller = controller
        self._dev = evdev.InputDevice(config.get("evdev", "path"))
        self.task = asyncio.ensure_future(self.main())

    async def main(self):
        async for event in self._dev.async_read_loop():
            if event.type == evdev.ecodes.EV_KEY:
                event = evdev.categorize(event)
                if event.keystate != event.key_down:
                    continue
                if type(event.keycode) == list:
                    keycode = event.keycode[-1]
                else:
                    keycode = event.keycode
                try:
                    code = keycode[4:]
                except IndexError:
                    code = keycode
                code = code.lower()
                self._handle_input(code)

    async def _toggle_power(self):
        if self._controller.denon_remote.enabled:
            powered = await self._controller.denon_remote.toggle_power()
            if powered:
                self._controller.player.start()
            else:
                self._controller.player.stop()
        else:
            self._controller.player.toggle_play()

    def _route_to_denon(self, command_name, player_fallback=True):
        if self._controller.denon_remote.enabled:
            asyncio.ensure_future(getattr(self._controller.denon_remote, command_name)())
        elif player_fallback:
            getattr(self._controller.player, command_name)()

    def _handle_input(self, code):
        if not self._remote_enabled and code != "red":
            return

        if code.startswith("numeric_"):
            idx = int(code[8:])
            self._controller.tune(idx - 1)
        elif code == "left":
            idx = self._controller.current_station_index()
            self._controller.tune(idx - 1)
        elif code == "right":
            idx = self._controller.current_station_index()
            self._controller.tune(idx + 1)
        elif code in ("pause", "play"):
            self._controller.player.toggle_play()
        elif code == "stop":
            self._controller.player.stop()
        elif code == "volumeup":
            self._route_to_denon(code)
        elif code == "volumedown":
            self._route_to_denon(code)
        elif code == "mute":
            self._route_to_denon("toggle_mute")
        elif code == "record":
            self._controller.player.toggle_recording()
        elif code == "power":
            asyncio.ensure_future(self._toggle_power())
        elif code == "red":
            if self._remote_enabled:
                self._controller.player.stop()
            else:
                self._controller.player.start()
            self._remote_enabled = not self._remote_enabled
        elif code in ("green", "yellow", "blue"):
            self._controller.tune_bookmarked_station(code)
        elif code == "forward":
            # I am running out of keys on this remote...
            self._route_to_denon("toggle_active_zone", False)
        elif code == "back":
            # I am running out of keys on this remote...
            self._route_to_denon("toggle_zone2_multi", False)
        else:
            print("Unhandled input: %s" % code)
