
def tune_if_active(controller, station_name):
    if controller.station_name == station_name:
        return

    player = controller.player
    if player.playing and not player.muted:
        controller.tune_station_with_name(station_name)

async def news_flash(controller):
    if controller.station_name == 'FIP':
        tune_if_active(controller, 'FranceInter')

async def fip(controller):
    if controller.station_name == 'FranceInter':
        tune_if_active(controller, 'FIP')

def schedule_tasks(controller, scheduler):
    # See the docs -> https://schedule.readthedocs.io/

    # Switch to FranceInter every hour.
    scheduler.every().hour.at(':00').do(news_flash, controller)

    # And 4 minutes later, back to FIP
    scheduler.every().hour.at(':04').do(fip, controller)
