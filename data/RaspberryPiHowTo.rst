
This mini-howto explains how to setup the player as a small daemon on
a Raspberry Pi running Raspbian.

Basic steps
===========

1. Install the player as explained in the README.rst
2. Make sure GStreamer works:

   ::

      $ gst-play-1.0 http://....

3. Create a config file:

   ::

      $ cp data/cocoricofm.cfg.sample ~/.config/cocoricofm.cfg
      # Edit file...

4. Enable the systemd service:

   ::

      $ mkdir -p ~/.config/systemd/user
      $ cp data/cocoricofm.service ~/.config/systemd/user/
      # Optionally edit service file (especially config file path)
      $ systemctl --user daemon-reload
      $ systemctl --user enable cocoricofm.service
      $ systemctl --user start cocoricofm.service

Growl
=====

If you want notifications delivered to your Mac via the network:

1. Install Growl on your Mac
2. Setup a password in the preferences
3. Make sure the Mac is reachable from the RPi by hostname
4. Install python-gntp on the RPi (no debian package, AFAIk. Use the
   source)
5. Update hostname/password options in cocoricofm.cfg
